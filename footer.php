<?php
/**
 * @package WordPress
 * @subpackage WP-Skeleton
 */
?>
  
      <br class="clear">
    <footer>
        <p>&copy; 2013 Patrick Serrano // v6.0</p>
    </footer>            
  </div>     

  <?php wp_footer(); ?>
  
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
</body>
</html>