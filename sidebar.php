<?php
/**
 * @package WordPress
 * @subpackage WP-Skeleton
 */
?>
    <div class="one-third column sidebar">
        <a href="<?php echo home_url(); ?>"><div class="logo"></div></a>
        <nav>
          <?php wp_nav_menu(array('menu' => 'Main Nav Menu')); ?>
        </nav>
        <hr>
        <section id="about-me">
          <h2>About Me</h2>
          <p>I'm Patrick Serrano, a designer &amp; developer from Long Island, NY. During the day I work as a designer and production manager for a small publishing company. At night I develop iOS apps, and make art.</p>
        </section>
        <hr>
        <?php if ( is_active_sidebar( 'right-sidebar' ) ) : ?> <?php dynamic_sidebar( 'right-sidebar' ); ?>
          <?php else : ?><p>You need to drag a widget into your sidebar in the WordPress Admin</p>
        <?php endif; ?>
        <hr>
        <div class="social">
          <a href="http://alpha.app.net/pats"><div id="app-net"></div></a>
          <a href="http://behance.net/patrickserrano"><div id="behance"></div></a>
          <a href="http://github.com/patrickserrano"><span id="github"></span></a>
        </div>
      </div>