function toggleApps() {
  if ($("#apps").is(":hidden")){
    $('#apps').slideDown('fast', function() {
      // Animation complete.
    })
  } else {
    $('#apps').slideUp('fast', function() {
      // Animation complete.
    });
  };
}

function toggleSketch() {
  if ($("#sketch").is(":hidden")){
    $('#sketch').slideDown('fast', function() {
      // Animation complete.
    })
  } else {
    $('#sketch').slideUp('fast', function() {
      // Animation complete.
    });
  };
}

function toggleTbp() {
  if ($("#tbp").is(":hidden")){
    $('#tbp').slideDown('fast', function() {
      // Animation complete.
    })
  } else {
    $('#tbp').slideUp('fast', function() {
      // Animation complete.
    });
  };
}

function toggleHeard() {
  if ($("#heard").is(":hidden")){
    $('#heard').slideDown('fast', function() {
      // Animation complete.
    })
  } else {
    $('#heard').slideUp('fast', function() {
      // Animation complete.
    });
  };
}

function togglePlayed() {
  if ($("#played").is(":hidden")){
    $('#played').slideDown('fast', function() {
      // Animation complete.
    })
  } else {
    $('#played').slideUp('fast', function() {
      // Animation complete.
    });
  };
}

function toggleSlices() {
  if ($("#slices").is(":hidden")){
    $('#slices').slideDown('fast', function() {
      // Animation complete.
    })
  } else {
    $('#slices').slideUp('fast', function() {
      // Animation complete.
    });
  };
}